echo "\e[1;34m<<<<<<<<<<<<<<<<<<<<<<<OpenCV Cross Installation>>>>>>>>>>>>>>>>>>>>>>>\e[0m"
sudo apt autoremove -y
sudo apt clean -y
sudo apt update -y
sudo apt upgrade -y

echo "\n\e[1;31m<<Installing Some OS Dependent Libraries>>\e[0m"
sudo apt update -y

sudo apt install libgtk-3-dev libcanberra-gtk3-dev -y

sudo apt install libtbb-dev qt5-default -y
sudo apt install libatlas-base-dev -y
sudo apt install libmp3lame-dev libtheora-dev -y
sudo apt install libvorbis-dev -y
sudo apt install libopencore-amrnb-dev libopencore-amrwb-dev -y
sudo apt install libavresample-dev -y
sudo apt install x264 v4l-utils -y

sudo apt install libtiff-dev zlib1g-dev -y
sudo apt install libjpeg-dev libpng-dev -y
sudo apt install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev -y
sudo apt install libxvidcore-dev libx264-dev -y

sudo apt install libprotobuf-dev protobuf-compiler -y
sudo apt install libgoogle-glog-dev libgflags-dev -y
sudo apt install libgphoto2-dev libeigen3-dev libhdf5-dev -y

echo "\n\e[1;31m<<Unpacking the installables>>\e[0m"
tar xfv CCOpenCV-4.1.0-armhf.tar.bz2 
sudo mv crosscompiledOpenCV /opt

echo "\n\e[1;31m<<Installing the package config details>>\e[0m"
sudo cp opencv.pc /usr/lib/arm-linux-gnueabihf/pkgconfig

echo "\n\e[1;31m<<Adding the Libraries to Library Path>>\e[0m"
echo 'export LD_LIBRARY_PATH=/opt/crosscompiledOpenCV/lib:$LD_LIBRARY_PATH' >> ~/.bashrc
alias brc='source ~/.bashrc'

echo "\n\e[1;31m<<Testing out OpenCV>>\e[0m"
cd tests_examples
g++ cli_opencv_test.cpp -o cli_opencv_test `pkg-config --cflags --libs opencv`
./cli_opencv_test

rm cli_opencv_test
rm lake_gray.jpg

cd ..

echo "\n\e[1;34m<<<<<<<<<<<<<<<<<<<OpenCV Installation Complete>>>>>>>>>>>>>>>>>>\e[0m"
