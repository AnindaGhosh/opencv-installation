echo "\e[1;34m<<<<<<<<<<<<<<<<<<<<<<<OpenCV Cross Compilation>>>>>>>>>>>>>>>>>>>>>>>\e[0m"
sudo apt autoremove -y
sudo apt clean -y
sudo apt update -y
sudo apt upgrade -y

echo "\n\e[1;31m<<Setting the Architecture>>\e[0m"
sudo dpkg --add-architecture armhf 

echo "\n\e[1;31m<<Installing Some OS Dependent Libraries>>\e[0m"
sudo apt update -y
sudo apt install qemu-user-static -y

sudo apt install libgtk-3-dev:armhf libcanberra-gtk3-dev:armhf -y

sudo apt install libtbb-dev:armhf qt5-default:armhf -y
sudo apt install libatlas-base-dev:armhf -y
sudo apt install libmp3lame-dev:armhf libtheora-dev:armhf -y
sudo apt install libvorbis-dev:armhf -y
sudo apt install libopencore-amrnb-dev:armhf libopencore-amrwb-dev:armhf -y
sudo apt install libavresample-dev:armhf -y
sudo apt install x264:armhf v4l-utils:armhf -y

sudo apt install libtiff-dev:armhf zlib1g-dev:armhf -y
sudo apt install libjpeg-dev:armhf libpng-dev:armhf -y
sudo apt install libavcodec-dev:armhf libavformat-dev:armhf libswscale-dev:armhf libv4l-dev:armhf -y
sudo apt install libxvidcore-dev:armhf libx264-dev:armhf -y

sudo apt install libprotobuf-dev:armhf protobuf-compiler:armhf -y
sudo apt install libgoogle-glog-dev:armhf libgflags-dev:armhf -y
sudo apt install libgphoto2-dev:armhf libeigen3-dev:armhf libhdf5-dev:armhf -y


echo "\n\e[1;31m<<Installing few Crossbuilding tools>>\e[0m"
sudo apt install crossbuild-essential-armhf -y
sudo apt install gfortran-arm-linux-gnueabihf -y

echo "\n\e[1;31m<<Installing CMake, Git, PackageConfig, Doxygen>>\e[0m"
sudo apt install cmake git pkg-config wget doxygen:armhf -y

echo "\n\e[1;31m<<Downloading OpenCV 4.1.0 & OpenCV Contrib Repo>>\e[0m"
wget -O opencv.tar.gz https://github.com/opencv/opencv/archive/4.1.0.tar.gz
tar xf opencv.tar.gz

wget -O opencv_contrib.tar.gz https://github.com/opencv/opencv_contrib/archive/4.1.0.tar.gz
tar xf opencv_contrib.tar.gz
rm *.tar.gz

export PKG_CONFIG_PATH=/usr/lib/arm-linux-gnueabihf/pkgconfig:/usr/share/pkgconfig
export PKG_CONFIG_LIBDIR=/usr/lib/arm-linux-gnueabihf/pkgconfig:/usr/share/pkgconfig

echo "\n\e[1;31m<<Create a folder for the compiled binaries>>\e[0m"
mkdir crosscompiledOpenCV
cd opencv-4.1.0
mkdir build
cd build

echo "\n\e[1;31m<<Prepare the CMake statement for compiling>>\e[0m"
cmake   -D CMAKE_BUILD_TYPE=RELEASE \
        -D CMAKE_INSTALL_PREFIX=../../crosscompiledOpenCV \
        -D CMAKE_TOOLCHAIN_FILE=../platforms/linux/arm-gnueabi.toolchain.cmake \
        -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib-4.1.0/modules \
        -D OPENCV_ENABLE_NONFREE=ON \
	-D INSTALL_C_EXAMPLES=ON \
        -D WITH_TBB=ON \
	-D WITH_V4L=ON \
        -D WITH_OPENGL=ON \
	-D ENABLE_NEON=ON \
        -D ENABLE_VFPV3=ON \
        -D BUILD_TESTS=OFF \
        -D BUILD_DOCS=ON ..

echo "\n\e[1;31m<<Start the compiling process, Sit back and relax it will take some time>>\e[0m"

make -j4

echo "\n\e[1;31m<<Now the cooking is done, Let's serve it>>\e[0m"
sudo make install

cd ../../

sudo rm -r opencv-4.1.0/ opencv_contrib-4.1.0/ crosscompiledOpenCV

echo "\n\e[1;31m<<Now we will pack the food for delivery, Check the compilation forlder for a tar.bz2 file>>\e[0m"
tar -cjvf CCOpenCV-4.1.0-armhf.tar.bz2 crosscompiledOpenCV

echo "\n\e[1;34m<<<<<<<<<<<<<<<<<<<OpenCV Cross Compilation Complete>>>>>>>>>>>>>>>>>>\e[0m"
