## Cross Compilation of OpenCV 4.1.0 for ARMhf based linux distro

**What's the need of cross compilation?**

Raspberry Pi 2/3 is a well known example of having ARMhf based core architecture. Genrally the ARM core architecture varies from each other like ARMv7 is different than ARMv8. The copilation of OpenCV takes significant amount of processing power and RAM. Since the memory footprint of these 
single board computers are low hence it's not advisable to do insitu compilation. Hence Cross Compilation. 
---

**Steps for Cross Compilation**

1. Setting up of a cross compilation developement environment.
2. Setting the cross compilation parameters using CMake.
3. Fetch the source code.
4. Cook the source code for creating the libraries.
5. Package the libraries together and make it ready for shipment. 
6. Dump the shipment in the ARMhf based distro and unpack it in proper places for further usage. 
---

**Setting up of the cross compilation development environment**

**Note** Before going to install the armhf based dependencies in your system, we will try not to mess with the HOST system. 

So, Lets create a VM first. Since my intended environment for usage is **Debian Buster (Raspbian Buster)**, I will create a VM of Debian Buster and will use the minimal OS configuration (No pre installed toolchains and all). 

**Debian Network Installer link for reference**
https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.3.0-amd64-netinst.iso


Install Debian buster in the Virtual Box (Or anything of your choice for VM creations).

Once we have the development setup done we will go through the steps 1 by 1.
---

## Steps for Cross Compilation

**First, We clean the package manager and update the developement system.**

sudo apt autoremove -y
sudo apt clean -y
sudo apt update -y
sudo apt upgrade -y

**Next, let’s enable the armhf architecture on the x86-64 machine**
sudo dpkg --add-architecture armhf 
sudo apt update -y
sudo apt install qemu-user-static -y

At this point, you should be able to install armhf libraries and applications on your system and run them.

**We are going to build OpenCV with support for C++ only. Next, we are going to install libgtk-3 in order to be able to write simple GUI programs. If you plan to use OpenCV exclusively on a headless system you can safely ignore the next two libraries**
sudo apt install libgtk-3-dev:armhf libcanberra-gtk3-dev:armhf -y

**Once we install the GTK libraries we will install some OS libraries for optimizing OpenCV**
sudo apt install libtbb-dev:armhf qt5-default:armhf -y
sudo apt install libatlas-base-dev:armhf -y
sudo apt install libmp3lame-dev:armhf libtheora-dev:armhf -y
sudo apt install libvorbis-dev:armhf -y
sudo apt install libopencore-amrnb-dev:armhf libopencore-amrwb-dev:armhf -y
sudo apt install libavresample-dev:armhf -y
sudo apt install x264:armhf v4l-utils:armhf -y

**We also need to install a bunch of other libraries required by OpenCV (various image and video formats support)**
sudo apt install libtiff-dev:armhf zlib1g-dev:armhf -y
sudo apt install libjpeg-dev:armhf libpng-dev:armhf -y
sudo apt install libavcodec-dev:armhf libavformat-dev:armhf libswscale-dev:armhf libv4l-dev:armhf -y
sudo apt install libxvidcore-dev:armhf libx264-dev:armhf -y

sudo apt install libprotobuf-dev:armhf protobuf-compiler:armhf -y
sudo apt install libgoogle-glog-dev:armhf libgflags-dev:armhf -y
sudo apt install libgphoto2-dev:armhf libeigen3-dev:armhf libhdf5-dev:armhf -y

**Next, we are going to install the default cross compilers from Debian which can be used to create armhf binaries for Raspberry Pi**
sudo apt install crossbuild-essential-armhf -y
sudo apt install gfortran-arm-linux-gnueabihf -y

At the time of this writing, the above toolchain is based on GCC 8.3, which is the same version of GCC available on Raspbian.

**Finally, we’ll install Cmake, git, pkg-config, wget and Doxygen**
sudo apt install cmake git pkg-config wget doxygen:armhf -y

**Next, we can download the current release of OpenCV. I will show you how to install the full OpenCV (default and contrib libraries)**
wget -O opencv.tar.gz https://github.com/opencv/opencv/archive/4.1.0.tar.gz
tar xf opencv.tar.gz

wget -O opencv_contrib.tar.gz https://github.com/opencv/opencv_contrib/archive/4.1.0.tar.gz
tar xf opencv_contrib.tar.gz
rm *.tar.gz


**We need to temporarily modify two system variables required to successfully build GTK+ support**
export PKG_CONFIG_PATH=/usr/lib/arm-linux-gnueabihf/pkgconfig:/usr/share/pkgconfig
export PKG_CONFIG_LIBDIR=/usr/lib/arm-linux-gnueabihf/pkgconfig:/usr/share/pkgconfig

**At this point, we can use Cmake to generate the OpenCV build scripts**
cmake   -D CMAKE_BUILD_TYPE=RELEASE \
        -D CMAKE_INSTALL_PREFIX=../../crosscompiledOpenCV \
        -D CMAKE_TOOLCHAIN_FILE=../platforms/linux/arm-gnueabi.toolchain.cmake \
        -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib-4.1.0/modules \
        -D OPENCV_ENABLE_NONFREE=ON \
	    -D INSTALL_C_EXAMPLES=ON \
        -D WITH_TBB=ON \
	    -D WITH_V4L=ON \
        -D WITH_OPENGL=ON \
	    -D ENABLE_NEON=ON \
        -D ENABLE_VFPV3=ON \
        -D BUILD_TESTS=OFF \
        -D BUILD_DOCS=ON ..

**Note** Check the Installation directory before running the CMake Command. Keep the TBB option on. We will use this for our future projects which will require for parallelization of the OpenCV Operations using Threading Building Blocks. NEON and VFPV3 are some of the platform based optimizing workaround. 
For reference on both visit https://processors.wiki.ti.com/index.php/Using_NEON_and_VFPv3_on_Cortex-A8

**Now it's time to cook the Binaries**
make -j4
sudo make install


**Note** make command takes the no of cpu cores so give -j(no of CPU cores u have in handy).
This step will take a while depending on the sonfiguration of your host machine. For me it was 4 Core, 4Gb RAM, Took 15 minutes to compile.



**Once, the binaries are ready we will pack in a .tar.bz2 file which we will copy to the Raspberry Pi SD card**
tar -cjvf CCOpenCV-4.1.0-armhf.tar.bz2 crosscompiledOpenCV


**Our Cross Compilation is done!!! Well Done**
---

## Steps for installation

Copy the binary repository in the Raspberry Pi's Home Directory

**Install the OS Libraries which will be needed for OpenCV to work properly**
sudo apt update -y

sudo apt install libgtk-3-dev libcanberra-gtk3-dev -y

sudo apt install libtbb-dev qt5-default -y
sudo apt install libatlas-base-dev -y
sudo apt install libmp3lame-dev libtheora-dev -y
sudo apt install libvorbis-dev -y
sudo apt install libopencore-amrnb-dev libopencore-amrwb-dev -y
sudo apt install libavresample-dev -y
sudo apt install x264 v4l-utils -y

sudo apt install libtiff-dev zlib1g-dev -y
sudo apt install libjpeg-dev libpng-dev -y
sudo apt install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev -y
sudo apt install libxvidcore-dev libx264-dev -y

sudo apt install libprotobuf-dev protobuf-compiler -y
sudo apt install libgoogle-glog-dev libgflags-dev -y
sudo apt install libgphoto2-dev libeigen3-dev libhdf5-dev -y

**Extract and Copy the binaries to the /opt folder**
tar xfv CCOpenCV-4.1.0-armhf.tar.bz2 
sudo mv crosscompiledOpenCV /opt

**Next, let’s also move opencv.pc where pkg-config can find it**
sudo mv opencv.pc /usr/lib/arm-linux-gnueabihf/pkgconfig

**In order for the OS to find the OpenCV libraries we need to add them to the library path**
echo 'export LD_LIBRARY_PATH=/opt/crosscompiledOpenCV/lib:$LD_LIBRARY_PATH' >> .bashrc
source .bashrc

**Log out and log in or restart the Terminal**

**Well Now it's time to test the installation**, I have provided a C++ project file in this repo which can test the installation in a Headless system also. Go under **tests_example/**.

Compile and Run the program--
g++ cli_cpp_test.cpp -o cli_cpp_test `pkg-config --cflags --libs opencv`
./cli_cpp_test
---

## Repo Structure

1. opencv-crosscompile.sh (To be run in the cross compiler environment)
2. opencv-installer.sh (To be run in Raspberry Pi)
3. CCOpenCV-4.1.0-armhf.tar.bz2 (Compiled Binaries for OpenCV 4.1.0)
4. opencv.pc (Package Config Files)
5. tests_examples (Cli Based Test Suite)
5. README.md (This File)
---

## End of the Article.
